# tessel-atmospheric
[![version 0.0.1](https://img.shields.io/badge/version-0.0.1-brightgreen.svg)](https://gitlab.com/yarthur/tessel-atmospheric/tree/0.0.1) [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)
> A basic atmospheric sensing project for Tessel 2.

## Contribute

This project is hosted at <https://gitlab.com/yarthur/tessel-atmospheric>. Please feel free to post any issues, questions, or feedback there, and I also welcome any PRs you may wish to offer.

## License

[MIT](LICENSE) © John Arthur

